Using this Project
----------------------

GradleKit is designed to automatically build workspaces full of projects,
by automatically wiring them up to see each other in a method similar to
catkin. To use, perform the following steps:

1. Create a new root folder which will hold all of your projects.

2. Clone GradleKit into a folder called `buildSrc` in the root folder.

3. Make a file called `settings.gradle` in the root folder that contains 
    a single line:
    
        `apply from: "buildSrc/root-settings.gradle"`
        
4. Create a gradle wrapper in the root folder. One way of doing this is
    to ask GradleKit to do it:
    
        `buildSrc/gradlew wrapper`
        
    You should now see `gradlew` and `gradlew.bat` in the root folder.

5. Clone projects into the root folder that you want to build.

6. To allow GradleKit to detect these projects add `gradlekit.gradle` to 
    the project folders, which can include custom logic these projects 
    need to build (e.g. dependencies).

7. To change the name GradleKit calls the project, add a 
   `gradlekit.properties` with `projectName=foo` next to the `gradlekit.gradle` 
   file. To change the platforms the project supports add e.g. 
   `platforms=common,js,jvm` to the properties file. 
   
7. run ./gradlew :project-name:build or such.
